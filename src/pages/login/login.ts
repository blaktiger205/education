import { Component, ViewChildren, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,ToastController ,App} from 'ionic-angular';
import { FirstPage } from '../first/first';
import { Storage } from '@ionic/storage';



/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
baseUrl: string = "http://dala.kelza.sa";
model: any ={};
toast:any;
loader = this.loadingCtrl.create({
  content: "Please wait...",
  duration: 3000
}); 

  constructor(private storage: Storage,
    public app:App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl:LoadingController,
    public toastCtrl: ToastController) {
  }
  createToast(message:string,callback:(() => void)) {
    this.toast = this.toastCtrl.create({
     message: message,
     duration: 3000,
     position: 'buttom'
   });   
 this.toast.onDidDismiss(() => {
   callback();
 });

 this.toast.present();
}
login() {
  this.baseUrl.post<any>("/api/Users/LoginApp", this.model).subscribe(data => {
    this.storage.set('userdata', data);
    this.loader.dismiss();
    this.rootPage(FirstPage, {});
  },
    error => {
      if (error.status == 409) {
        this.createToast(error.error, () => { })
        this.loader.dismiss();
      }
    });
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
 
}
